# apiref

API reference for communicating with the tarnishwm compositor backend.


## What is this for? ##

TarnishWM is basically set up like this:

```
|------------|
| Compositor |
|------------|
 | ^    ^  |
 | |    |  |
REPLY-REQUEST
data  command
\/ |    |  \/
|------------|
|   tarnd    |
|------------|
```

The compositor is **just a wayland compositor.**
It doesn't handle keyboard mapping, theme engine, or any
of that fancy stuff. For that functionality, the compositor
outsources to `tarnd`, a configurable Python daemon which
handles keyboard maps, layouts, etc.

`tarnd` and the compositor communicate via a zeroMQ REPLY stream
on tcp://localhost:5555 and a REQUEST stream on
tcp://localhost:5556. The REPLY stream is mainly for receiving data
from the compositor (such as keypresses or window data). The REQUEST
stream is mainly for sending messages to the compositor (such as a
LAUNCHCMD).

### Yeah, but what is this for? ###
Hold on, I'm getting there.

`tarnd` and the compositor need a standard, easy to extend
message format API that allows them to understand each other.
This is because:

1. Nobody wants to hardcode messages for every single message
2. It'll be easier for humans to understand what the formats are
3. Easier to debug
4. It's just better

This project contains a reference XML API for creating message
protocols for `tarnd`-compositor communication. (It also
contains docs.)

An example of a fully defined protocol is in ref/shell/shell.xml.
It should be fairly self-explanatory.

Have fun :)
