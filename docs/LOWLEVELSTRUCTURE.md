# tarnd-compositor Standard Message Structure
Every tarnd-compositor message will follow this format:

`{struct(int code, int value, int flags)}[string or some other blob]`

Some notes:
- ID codes are defined in their respective XML protocol files.
- The blob does not have to exist. The message decoder will infer
whether there is a blob or not based on the code.


Yes, Kalyan, we'll put it in the wiki later, hold yer horses.
